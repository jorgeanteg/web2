<?php
    class Instructor extends CI_Model
    {
        function __construct()
        {
            parent::__construct();
        }

        //funcion insertar un instructor
        function insertar($datos){
            //ACTIVE RECORD -> CODEiGNITER
            return $this->db->insert("instructor",$datos);
        }

        //FUncion para consultar Instructores 
        function obtenerTodos(){
            $listadoIntructores=$this->db->get("instructor");//esto devuelve un array
            if($listadoIntructores->num_rows()>0) { //si hay datos
                return $listadoIntructores->result();
            }else{ //si no hay datos
                return false;
            }
        }

        //Borrar INstructor
        function borrar($id_ins){
            //"id_ins"-> es el campo de la base de datos  y la $id_ins es la variable que creamos puede tener otro nombre
            $this->db->where("id_ins",$id_ins);

            //instructor tabla de base de datos
            if ($this->db->delete("instructor")) {
                return true;
            } else {
                return false;
            }
            
            //El codigo de arriba en una solo linea
            // return $this->db->delete("instructor");
        }

    } //cierre de la clase


?>