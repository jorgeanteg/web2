<?php
    class EStudiante extends CI_Model
    {
        function __construct()
        {
            parent::__construct();
        }

        //funcion insertar un instructor
        function insertar($datos){
            //ACTIVE RECORD -> CODEiGNITER
            return $this->db->insert("estudiante",$datos);
        }


        function obtenerTodos(){
            $listadoEstudiantes=$this->db->get("estudiante");//esto devuelve un array
            if($listadoEstudiantes->num_rows()>0) { //si hay datos
                return $listadoEstudiantes->result();
            }else{ //si no hay datos
                return false;
            }
        }

        function borrar($id_est){
            
            $this->db->where("id_est",$id_est);


            if ($this->db->delete("estudiante")) {
                return true;
            } else {
                return false;
            }

        }


    } //cierre de la clase


?>