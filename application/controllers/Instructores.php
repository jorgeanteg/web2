<?php 

    class Instructores extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();

            //Cargar modelo
            $this->load->model('Instructor');
        }

        //Funcion que renderiza la vista index
        
        public function index(){

            //data es un nombre cualquiera
            $data['instructores']=$this->Instructor->obtenerTodos();
            
            $this->load->view('header');
            // estamos pasando los datos a la vista 
            $this->load->view('instructores/index', $data);
            $this->load->view('footer');

        } 
        //Funcion que renderiza la vista nuevo
        public function nuevo(){
            $this->load->view('header');
            $this->load->view('instructores/nuevo');
            $this->load->view('footer');
        }

        public function guardar() {
            $datosNuevoInstructor=array(
                "cedula_ins"=>$this->input->post('cedula_ins'),
                "primer_apellido_ins"=>$this->input->post('primer_apellido_ins'),
                "segundo_apellido_ins"=>$this->input->post('segundo_apellido_ins'),
                "nombres_ins"=>$this->input->post('nombres_ins'),
                "titulo_ins"=>$this->input->post('titulo_ins'),
                "telefono_ins"=>$this->input->post('telefono_ins'),
                "direccion_ins"=>$this->input->post('direccion_ins'),
            );
            
            //llamamos a insertar

            if($this->Instructor->insertar($datosNuevoInstructor)){
                redirect('instructores/index');
            }else{
                echo "<h1> Error al insertar</h1>";
            }

            

            
            // es el nom,bre del formulario el name de cada input
            // echo $this->input->post('cedula_ins'); 
            // echo "<br>";
            // echo $this->input->post('primer_apellido_ins'); 
            // echo "<br>";
            // echo $this->input->post('segundo_apellido_ins'); 
            // echo "<br>";
            // echo $this->input->post('nombres_ins'); 
            // echo "<br>";
            // echo $this->input->post('titulo_ins'); 
            // echo "<br>";
            // echo $this->input->post('telefono_ins'); 
            // echo "<br>";
            // echo $this->input->post('direccion_ins');  
        }

        //FUncion para eliminar instructores
        //Empleamos el metodo get
        public function eliminar($id_ins){
            if ($this->Instructor->borrar($id_ins)) {
                redirect('instructores/index');
                # code...
            } else {
                # code...
                echo "ERROR AL BORRAR :(";
            }
            
        }
    }//Cierre d ela clase<





?>