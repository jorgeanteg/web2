<?php 

    class Estudiantes extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();

            //Cargar modelo
            $this->load->model('Estudiante');
        }

        //Funcion que renderiza la vista index
        
        public function listado(){

            //data es un nombre cualquiera
            $data['estudiantes']=$this->Estudiante->obtenerTodos();
            
            $this->load->view('header');
            // estamos pasando los datos a la vista 
            $this->load->view('estudiantes/listado', $data);
            $this->load->view('footer');
        } 

        
        //Funcion que renderiza la vista nuevo
        public function nuevo(){
            $this->load->view('header');
            $this->load->view('estudiantes/nuevo');
            $this->load->view('footer');
        }

        public function guardar() {
            $datosNuevoEstudiante=array(
                "cedula_est"=>$this->input->post('cedula_est'),
                "apellidos_est"=>$this->input->post('apellidos_est'),
                "nombres_est"=>$this->input->post('nombres_est'),
                "ciclo_est"=>$this->input->post('ciclo_est'),
                "tipo_sangre_est"=>$this->input->post('tipo_sangre_est'),
                "telefono_est"=>$this->input->post('telefono_est'),
                "fecha_nacimiento_est"=>$this->input->post('fecha_nacimiento_est'),
                "observacion_est"=>$this->input->post('observacion_est'),
                "direccion_est"=>$this->input->post('direccion_est'),
            );

            if($this->Estudiante->insertar($datosNuevoEstudiante)){
                redirect('estudiantes/listado');
            }else{
                echo "<h1> Error al insertar</h1>";
            }
            
        
        }

        //FUncion para eliminar estudiantes
        //Empleamos el metodo get
        public function eliminar($id_est){
            if ($this->Estudiante->borrar($id_est)) {
                redirect('estudiantes/listado');
                # code...
            } else {
                # code...
                echo "ERROR AL BORRAR :(";
            }
            
        }
    }

?>