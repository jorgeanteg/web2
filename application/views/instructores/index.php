<style>
    .bton-nuevo {
        margin-top: 18px;
    }
</style>

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <h1 class="text-center">LISTADO DE INSTRUCTORES</h1>
        </div>
        <div class="col-md-4 bton-nuevo">
            <a href="<?php echo site_url(); ?>/instructores/nuevo" class="btn btn-primary" >
                <i class="glyphicon glyphicon-plus" > </i>
                Agregar Instructor
            </a>
        </div>
    </div>
    <br>
    <?php if ($instructores): ?>
        <table class="table table-striped table-bordered table-hover">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>CÉDULA</th>
                    <th>PRIMER APELLIDO</th>
                    <th>SEGUNGO APELLIDO</th>
                    <th>NOMBRES</th>
                    <th>TÍTULO</th>
                    <th>TELÉFONO</th>
                    <th>DIRECCIÓN</th>
                    <th>ACCIONES</th>
                </tr>
            </thead>


            <tbody>
                <?php foreach ($instructores as $filaTemporal): ?>
                    <tr>
                        <td>
                            <?php echo $filaTemporal->id_ins; ?>
                        </td>
                        <td>
                            <?php echo $filaTemporal->cedula_ins; ?>
                        </td>
                        <td>
                            <?php echo $filaTemporal->primer_apellido_ins; ?>
                        </td>
                        <td>
                            <?php echo $filaTemporal->segundo_apellido_ins; ?>
                        </td>
                        <td>
                            <?php echo $filaTemporal->nombres_ins; ?>
                        </td>
                        <td>
                            <?php echo $filaTemporal->titulo_ins; ?>
                        </td>
                        <td>
                            <?php echo $filaTemporal->telefono_ins; ?>
                        </td>

                        <td>
                            <?php echo $filaTemporal->direccion_ins; ?>
                        </td>
                        <td class="text-center">
                            <a href="#" title="Editar Instructor">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </a>
                            &nbsp;&nbsp;&nbsp;
                            <a href="<?php echo site_url(); ?>/instructores/eliminar/<?php echo $filaTemporal->id_ins; ?>"
                                title="Eliminar Instructor" onclick="return confirm('¿Está seguro de eliminar este registro?');" style="color:red;">
                                <i class="glyphicon glyphicon-trash"></i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>


        </table>

    <?php else: ?>
        <h1>No hay instructores</h1>
    <?php endif; ?>
</div>


<!-- sentencias selectivas
->if
->switch / case

sentencias iterativas
-> for
-> while
-> do while
-> foreach -->