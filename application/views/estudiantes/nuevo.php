<div class="container">
    <H1 class="text-center">NUEVO ESTUDIANTES</H1>
    <form class="" action="<?php echo site_url(); ?>/estudiantes/guardar" method="post">
        <div class="row">
            <div class="col-md-4">
                <label for="">Cédula:</label>
                <br>
                <input type="number" placeholder="Ingrese la cédula" class="form-control" name="cedula_est" value=""
                    id="cedula_est">
            </div>
            <div class="col-md-4">
                <label for="">Apellidos:</label>
                <br>
                <input type="text" placeholder="Ingrese los apellidos" class="form-control"
                    name="apellidos_est" value="" id="apellidos_est">
            </div>
            <div class="col-md-4">
                <label for="">Nombres:</label>
                <br>
                <input type="text" placeholder="Ingrese sus nombres" class="form-control"
                    name="nombres_est" value="" id="nombres_est">
            </div>
        </div>
        <br>
        <div class="row">
            <div class="col-md-4">
                <label for="">Ciclo:</label>
                <br>
                <input type="text" placeholder="Ingrese el ciclo que cursa" class="form-control" name="ciclo_est" value=""
                    id="ciclo_est">
            </div>
            <div class="col-md-4">
                <label for="">Tipo de sangre:</label>
                <br>
                <input type="text" placeholder="Ingrese su tipo de sangre" class="form-control" name="tipo_sangre_est" value=""
                    id="tipo_sangre_est">
            </div>
            <div class="col-md-4">
                <label for="">Teléfono:</label>
                <br>
                <input type="number" placeholder="Ingrese el telefono" class="form-control" name="telefono_est" value=""
                    id="telefono_est">
            </div>
        </div>

        <br>
        <div class="row">
            <div class="col-md-4">
                <label for="">Fecha de Nacimiento:</label>
                <br>
                <input type="date" placeholder="Ingrese la Fecha de Nacimiento" class="form-control" name="fecha_nacimiento_est" value=""
                    id="fecha_nacimiento_est">
            </div>
            <div class="col-md-8">
                <label for="">Observación:</label>
                <br>
                <input type="text" placeholder="Ingrese una Observación" class="form-control" name="observacion_est" value=""
                    id="observacion_est">
            </div>
        </div>

        <br>
        <div class="row">
            <div class="col-md-12">
                <label for="">Dirección:</label>
                <br>
                <input type="text" placeholder="Ingrese la direccion" class="form-control" name="direccion_est" value=""
                    id="direccion_est">
            </div>
        </div>

        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-primary">
                    Guardar
                </button>
                &nbsp;
                <a href="<?php echo site_url(); ?>/estudiantes/listado" class="btn btn-danger">
                    Cancelar
                </a>
            </div>
        </div>
    </form>


</div>