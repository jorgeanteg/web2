<div class="container">

    <div class="row">
        <div class="col-md-8">
            <h1 class="text-center">LISTADO DE ESTUDIANTES</h1>
        </div>
        <div class="col-md-4 bton-nuevo">
            <a href="<?php echo site_url(); ?>/instructores/nuevo" class="btn btn-primary">
                <i class="glyphicon glyphicon-plus"> </i>
                Agregar Estudiante
            </a>
        </div>
    </div>


    <br>
    <?php if ($estudiantes): ?>
        <table class="table table-striped table-bordered">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>CÉDULA</th>
                    <th>APELLIDOs</th>
                    <th>NOMBRES</th>
                    <th>CICLO</th>
                    <th>TIPO DE SANGRE</th>
                    <th>TELEFONO</th>
                    <th>FECHA DE NACIMIENTO</th>
                    <th>OBSERVACIÓN</th>
                    <th>DIRECCION</th>
                </tr>
            </thead>


            <tbody>
                <?php foreach ($estudiantes as $filaTemporal): ?>
                    <tr>
                        <td>
                            <?php echo $filaTemporal->id_est; ?>
                        </td>
                        <td>
                            <?php echo $filaTemporal->cedula_est; ?>
                        </td>
                        <td>
                            <?php echo $filaTemporal->apellidos_est; ?>
                        </td>
                        <td>
                            <?php echo $filaTemporal->nombres_est; ?>
                        </td>
                        <td>
                            <?php echo $filaTemporal->ciclo_est; ?>
                        </td>
                        <td>
                            <?php echo $filaTemporal->tipo_sangre_est; ?>
                        </td>
                        <td>
                            <?php echo $filaTemporal->telefono_est; ?>
                        </td>

                        <td>
                            <?php echo $filaTemporal->fecha_nacimiento_est; ?>
                        </td>

                        <td>
                            <?php echo $filaTemporal->observacion_est; ?>
                        </td>

                        <td>
                            <?php echo $filaTemporal->direccion_est; ?>
                        </td>

                        <td class="text-center">
                            <a href="#" title="Editar EStudiante">
                                <i class="glyphicon glyphicon-pencil"></i>
                            </a>
                            &nbsp;&nbsp;&nbsp;
                            <a href="<?php echo site_url(); ?>/estudiantes/eliminar/<?php echo $filaTemporal->id_est; ?>"
                                title="Eliminar EStudiante" onclick="return confirm('¿Está seguro de eliminar este registro?');" style="color:red;">
                                <i class="glyphicon glyphicon-trash"></i>
                            </a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>


        </table>

    <?php else: ?>
        <h1>No hay instructores</h1>
    <?php endif; ?>
</div>